<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use MyHotelBike\Translations\Http\Controllers\Api\TranslationController;

$translation = \MyHotelBike\Translations\Models\Translation::SINGULAR;
$translation_route = Str::slug(__("translations::models.$translation.plural"));
Route::get("$translation_route", [TranslationController::class, 'index']);
