<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use MyHotelBike\Translations\Http\Controllers\LocaleController;
use MyHotelBike\Translations\Http\Controllers\TranslationController;

$locale = 'locale';
$locale_route = 'locales';
Route::get("$locale_route", [LocaleController::class, 'index'])->name("$locale.index");
Route::get("$locale_route/create", [LocaleController::class, 'create'])->name("$locale.create");
Route::post("$locale_route/", [LocaleController::class, 'store'])->name("$locale.store");
Route::delete("$locale_route/{{$locale}}", [LocaleController::class, 'destroy'])->name("$locale.destroy");

$translation = \MyHotelBike\Translations\Models\Translation::SINGULAR;
$translation_route = Str::slug(__("translations::models.$translation.plural"));
Route::get("$translation_route", [TranslationController::class, 'index'])->name("$translation.index");
Route::any("$translation_route/data", [TranslationController::class, 'data'])->name("$translation.data");
Route::get("$translation_route/{{$translation}}/edit", [TranslationController::class, 'edit'])->name("$translation.edit");
Route::put("$translation_route/{{$translation}}", [TranslationController::class, 'update'])->name("$translation.update");
