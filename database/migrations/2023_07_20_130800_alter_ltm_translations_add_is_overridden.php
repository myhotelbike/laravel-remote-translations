<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLtmTranslationsAddIsOverridden extends Migration {

    public function up() {
        Schema::table('ltm_translations', function(Blueprint $table) {
            $table->boolean('is_overridden')->default(FALSE);
        });
    }

    public function down() {
        Schema::table('ltm_translations', function(Blueprint $table) {
            $table->dropColumn('is_overridden');
        });
    }
}
