<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLtmTranslationsAddUnique extends Migration {

    public function up() {
        Schema::table('ltm_translations', function(Blueprint $table) {
            $table->unique(['locale', 'group', 'key']);
        });
    }

    public function down() {
        Schema::table('ltm_translations', function(Blueprint $table) {
            $table->dropUnique(['locale', 'group', 'key']);
        });
    }
}
