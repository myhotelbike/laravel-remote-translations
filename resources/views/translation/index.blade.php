@push('js')
    <script>
        window.addEventListener('DOMContentLoaded', function () {
            window.ticket_index = $('#translation-index').DataTable(support.dictMerge(tables.dataTablesSettings, {
                ajax: {
                    url: '{{ route('translation.data') }}',
                    method: 'POST',
                },
                columns: [
                    {
                        data: 'locale',
                    },
                    {
                        data: 'group',
                    },
                    {
                        data: 'key',
                    },
                    {
                        data: 'value',
                        render: function (data, type, row) {
                            var str = data;

                            if (row.is_overridden) {
                                str += ' <span class="badge badge-danger">Overridden</span>';
                            }

                            return str;
                        }
                    },
                    {
                        data: 'edit_url',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row) {
                            var str = '';

                            if (data) {
                                str += '<a href="' + data + '" class="btn btn-light ml-2" dusk="translation:' + row.id + ':edit">'
                                    + '@lang('translations::models.translation.actions.edit')'
                                    + '</a>';
                            }

                            return str;
                        }
                    }
                ],
                order: [[0, 'asc'], [1, 'asc'], [2, 'asc']],
                serverSide: true,
                createdRow: function (row, data, dataIndex) {
                    row.id = 'translation-index-' + data.id;

                    $('td:last-child', $(row)).addClass('nowrap');
                }
            }));
        });
    </script>
@endpush

<x-app-layout>
    <x-slot name="header">
        <h2>
            {{ __('translations::models.translation.plural') }}
        </h2>
    </x-slot>

    <div class="card">
        <div class="card-body text-right">
            <a href="{{ route('locale.index') }}" class="btn btn-light">
                @lang('translations::models.locale.plural')
            </a>
        </div>
    </div>

    <table id="translation-index" class="table table-striped">
        <caption class="sr-only">@lang('models.ticket.plural')</caption>
        <thead>
        <tr>
            <th>@lang('translations::models.translation.attributes.locale')</th>
            <th>@lang('translations::models.translation.attributes.group')</th>
            <th>@lang('translations::models.translation.attributes.key')</th>
            <th>@lang('translations::models.translation.attributes.value')</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</x-app-layout>
