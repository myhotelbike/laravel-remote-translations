<x-app-layout>
    <x-slot name="header">
        <h2>{{ $translation->group }} {{ $translation->key }}</h2>
    </x-slot>

    <x-form method="PUT" :action="route('translation.update', $translation)">
        @foreach ($locales as $locale)
            <x-form-group :for="$locale.'-value'" :label="$locale">
                <x-input :id="$locale.'-value'" type="text" :name="$locale.'[value]'"
                         :value="old($locale.'.value', optional($translations->get($locale))->value)"/>
            </x-form-group>
        @endforeach

        <div class="text-right">
            <x-submit>
                {{ __('Save') }}
            </x-submit>
        </div>
    </x-form>
</x-app-layout>
