<x-app-layout>
    <x-slot name="header">
        <h2>
            {{ __('translations::models.locale.plural') }}
        </h2>
    </x-slot>

    @can('create', \MyHotelBike\Translations\Models\Translation::class)
        <div class="card">
            <div class="card-body text-right">
                <a href="{{ route('locale.create') }}" class="btn btn-light">
                    @lang('translations::models.locale.actions.create')
                </a>
            </div>
        </div>
    @endcan

    <table id="locale-index" class="table table-striped">
        <caption class="sr-only">@lang('models.locale.plural')</caption>
        <thead>
        <tr>
            <th>@lang('translations::models.locale.attributes.locale')</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($locales as $locale)
            <tr>
                <td>{{ $locale }}</td>
                <td>
                    @can('translation.*.destroy')
                        <a href="{{ route('locale.destroy', $locale) }}"
                           class="btn btn-light"
                           data-method="DELETE"
                           data-confirm="@lang('Are you sure you want to :action :model?', ['action' => __('translations::models.locale.actions.destroy'), 'model' => $locale])">
                            @lang('translations::models.locale.actions.destroy')
                        </a>
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-app-layout>
