<x-app-layout>
    <x-slot name="header">
        <h2>@lang('translations::models.locale.actions.create')</h2>
    </x-slot>

    <x-form method="POST" :action="route('locale.store')">
        <x-form-group for="locale" :label="__('translations::models.locale.attributes.locale')">
            <x-input id="locale" type="text" name="locale" :value="old('locale')"/>
        </x-form-group>

        <div class="text-right">
            <x-submit>
                {{ __('Save') }}
            </x-submit>
        </div>
    </x-form>
</x-app-layout>
