<?php

return [
    'locale'      => [
        'name'       => 'Locale',
        'plural'     => 'Locales',
        'actions'    => [
            'index'   => 'Locale history',
            'create'  => 'Add locale',
            'show'    => 'Details',
            'edit'    => 'Edit',
            'delete'  => 'Delete',
            'destroy' => 'Delete',
            'restore' => 'Restore',
            'admin'   => 'Admin',
            'filter'  => 'Filter locales',
        ],
        'mutations'  => [
            'create'   => ':Employee created locale',
            'edit'     => ':Employee updated locale',
            'delete'   => ':Employee deleted locale',
            'attached' => ':Employee attached',
            'detached' => ':Employee detached',
        ],
        'attributes' => [
            'locale' => 'Locale',
        ],
    ],
    'translation' => [
        'name'       => 'Translation',
        'plural'     => 'Translations',
        'actions'    => [
            'index'   => 'Translation history',
            'create'  => 'Add translation',
            'show'    => 'Details',
            'edit'    => 'Edit',
            'delete'  => 'Delete',
            'destroy' => 'Delete',
            'restore' => 'Restore',
            'admin'   => 'Admin',
            'filter'  => 'Filter translations',
        ],
        'mutations'  => [
            'create'   => ':Employee created translation',
            'edit'     => ':Employee updated translation',
            'delete'   => ':Employee deleted translation',
            'attached' => ':Employee attached',
            'detached' => ':Employee detached',
        ],
        'attributes' => [
            'locale' => 'Locale',
            'group'  => 'Group',
            'key'    => 'Key',
            'value'  => 'Value',
        ],
    ],
];
