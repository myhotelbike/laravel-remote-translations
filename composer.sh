#!/usr/bin/env bash

docker run --rm\
  --entrypoint=""\
  --volume "$(pwd):/var/www/html"\
  registry.gitlab.com/myhotelbike/laravel-build:master\
  su-exec web /usr/local/bin/php -d memory_limit=-1 /usr/bin/composer "$@"
