<?php

return [
    'web'    => [
        'domain'     => NULL,
        'path'       => 'laravel-translations',
        'middleware' => ['web', 'auth'],
        'name'       => '',
    ],
    'api'    => [
        'domain'     => NULL,
        'path'       => 'api/laravel-translations',
        'middleware' => ['api', 'auth:sanctum'],
        'name'       => '',
    ],
    'client' => [
        'host'  => env('TRANSLATIONS_HOST'),
        'token' => env('TRANSLATIONS_TOKEN'),
    ],
];
