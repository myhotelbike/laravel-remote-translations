#!/usr/bin/env bash

docker run --rm\
  --entrypoint=""\
  --volume "$(pwd):/var/www/html"\
  registry.gitlab.com/myhotelbike/laravel-build:master\
  su-exec web vendor/bin/phpunit "$@"
