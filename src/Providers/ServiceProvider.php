<?php

namespace MyHotelBike\Translations\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Route;
use MyHotelBike\LaravelSupport\Events\Deployed;
use MyHotelBike\Translations\Listeners\DeployedHandleTranslations;

class ServiceProvider extends \Illuminate\Support\ServiceProvider {

    public function boot() {
        $configPath = __DIR__ . '/../../config/translations.php';
        $this->mergeConfigFrom($configPath, 'translations');
        $this->publishes([$configPath => config_path('translations.php')], 'config');

        $translationPath = __DIR__ . '/../../resources/lang';
        $this->loadTranslationsFrom($translationPath, 'translations');
        $this->publishes([$translationPath => $this->app->langPath('vendor/translations')], 'translations');

        $viewPath = __DIR__ . '/../../resources/views';
        $this->loadViewsFrom($viewPath, 'translations');
        $this->publishes([$viewPath => resource_path('views/vendor/translations')], 'views');

        $migrationPath = __DIR__ . '/../../database/migrations';
        $this->loadMigrationsFrom($migrationPath);

        Route::domain(config('translations.web.domain'))
            ->prefix(config('translations.web.path'))
            ->middleware(config('translations.web.middleware'))
            ->name(config('translations.web.name'))
            ->group(function() {
                $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
            });

        Route::domain(config('translations.api.domain'))
            ->prefix(config('translations.api.path'))
            ->middleware(config('translations.api.middleware'))
            ->name(config('translations.api.name'))
            ->group(function() {
                $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
            });

        Relation::morphMap([
            \MyHotelBike\Translations\Models\Translation::SINGULAR => \MyHotelBike\Translations\Models\Translation::class,
        ]);

        $this->commands([
            \MyHotelBike\Translations\Console\Commands\TranslationsSync::class,
            \MyHotelBike\Translations\Console\Commands\TranslationsUpdate::class,
        ]);

        Event::listen(Deployed::class, DeployedHandleTranslations::class);
    }

    public function register() {
        $this->app->singleton(\MyHotelBike\Translations\Contracts\TranslationsUpdateManager::class, \MyHotelBike\Translations\Support\UpdateManager::class);
        $this->app->singleton(\MyHotelBike\Translations\Contracts\TranslationsSyncManager::class, \MyHotelBike\Translations\Support\SyncManager::class);
    }
}
