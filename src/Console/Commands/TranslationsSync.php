<?php

namespace MyHotelBike\Translations\Console\Commands;

use Illuminate\Console\Command;
use MyHotelBike\Translations\Contracts\TranslationsSyncManager;

class TranslationsSync extends Command {
    protected $signature = 'translations:sync';

    protected $description = 'Download the current translations from the host';

    public function handle(TranslationsSyncManager $manager): int {
        $result = $manager->sync();

        if ($result) {
            $this->info("$result translations synced.");
        }

        return 0;
    }
}
