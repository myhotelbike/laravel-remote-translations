<?php

namespace MyHotelBike\Translations\Console\Commands;

use Illuminate\Console\Command;
use MyHotelBike\Translations\Contracts\TranslationsUpdateManager;

class TranslationsUpdate extends Command {
    protected $signature = 'translations:update';

    protected $description = 'Export changed translations';

    public function handle(TranslationsUpdateManager $manager): int {
        $result = $manager->update();

        if ($result) {
            $this->info("$result translation files updated.");
        }

        return 0;
    }
}
