<?php


namespace MyHotelBike\Translations\Support;

use Barryvdh\TranslationManager\Manager as BaseManager;
use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Application;
use MyHotelBike\Translations\Contracts\TranslationsUpdateManager;
use MyHotelBike\Translations\Models\Translation;

class UpdateManager implements TranslationsUpdateManager {

    protected Application $app;
    protected Filesystem $files;
    protected BaseManager $manager;

    public function __construct(Application $app, Filesystem $files, BaseManager $manager) {
        $this->app = $app;
        $this->files = $files;
        $this->manager = $manager;
    }

    public function update(): int {
        $result = 0;
        $groups = Translation::query()
            ->select(['group', 'locale'])
            ->selectRaw('max(updated_at) as updated_at')
            ->groupBy('group', 'locale')
            ->get();

        foreach ($groups as $group) {
            if (!$this->shouldUpdate($group)) {
                continue;
            }

            if ($group->group == BaseManager::JSON_GROUP) {
                $this->manager->exportTranslations(NULL, TRUE);
            }
            else {
                $this->manager->exportTranslations($group->group, FALSE);
            }

            $result++;
        }

        return $result;
    }

    public function shouldUpdate(Translation $group): bool {
        $path = $this->getPathForGroup($group);
        if (!$this->files->exists($path)) {
            return TRUE;
        }

        $file_updated_at = Carbon::createFromTimestamp($this->files->lastModified($path), config('app.timezone'));

        return $group->updated_at->greaterThan($file_updated_at);
    }

    public function getPathForGroup(Translation $group): string {
        $base_path = $this->app['path.lang'];

        if ($group->group === BaseManager::JSON_GROUP) {
            return "{$base_path}/{$group->locale}.json";
        }

        if (str_starts_with($group->group, 'vendor/')) {
            $parts = explode('/', $group->group);

            return "{$base_path}/{$parts[0]}/{$parts[1]}/{$group->locale}/{$parts[2]}.php";
        }

        return "{$base_path}/{$group->locale}/{$group->group}.php";
    }
}
