<?php


namespace MyHotelBike\Translations\Support;

use Barryvdh\TranslationManager\Manager as BaseManager;
use Carbon\Carbon;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use MyHotelBike\Translations\Contracts\TranslationsSyncManager;
use MyHotelBike\Translations\Models\Translation;

class SyncManager implements TranslationsSyncManager {

    const CACHE_KEY = 'translations.synced_at.';

    public function __construct(
        protected readonly BaseManager $manager,
        protected readonly Repository $cache,
    ) {
    }

    public function sync(): int {
        $result = 0;

        foreach ($this->manager->getLocales() as $locale) {
            $result += $this->syncLocale($locale);
        }

        return $result;
    }

    public function syncLocale(string $locale): int {
        $result = 0;
        $synced_at = $this->getSyncedAt($locale)?->format(\DateTimeInterface::ATOM);

        do {
            $response = Http::withToken(config('translations.client.token'))
                ->acceptJson()
                ->get(config('translations.client.host') . 'translations', [
                    'limit'      => 100,
                    'offset'     => $result,
                    'locale'     => $locale,
                    'updated_at' => $synced_at,
                ])
                ->throw()
                ->json();

            $inserts = [];
            foreach ($response['translations'] as $translation) {
                $inserts[] = [
                    'group'      => $translation['group'],
                    'key'        => $translation['key'],
                    'locale'     => $translation['locale'],
                    'value'      => $translation['value'],
                    'created_at' => $translation['created_at'],
                    'updated_at' => $translation['updated_at'],
                ];
            }

            if (!empty($inserts)) {
                $query = Translation::query()->getQuery();
                $query = $query->grammar->compileInsert($query, $inserts);
                $query .= ' ON CONFLICT ("locale", "group", "key") DO UPDATE SET';
                $query .= ' "value" = EXCLUDED."value",';
                $query .= ' "updated_at" = EXCLUDED."updated_at"';
                $query .= ' WHERE ltm_translations.is_overridden = false';
                DB::connection()->insert($query, Arr::flatten($inserts, 1));
            }
        }while (!empty($response['translations']) && $response['total'] > $result += count($inserts));

        $this->setSyncedAt($locale, Carbon::now());

        return $result;
    }

    public function getSyncedAt(string $locale): ?Carbon {
        return $this->cache->get(self::CACHE_KEY . $locale);
    }

    public function setSyncedAt(string $locale, Carbon $synced_at): void {
        $this->cache->put(self::CACHE_KEY . $locale, $synced_at);
    }
}
