<?php

namespace MyHotelBike\Translations\Listeners;

use Barryvdh\TranslationManager\Console\ExportCommand;
use Barryvdh\TranslationManager\Console\ImportCommand;
use Illuminate\Support\Facades\Artisan;
use MyHotelBike\LaravelSupport\Enums\DeployType;
use MyHotelBike\LaravelSupport\Events\Deployed;

class DeployedHandleTranslations {
    public function handle(Deployed $event) {
        if ($event->app->environment('local')) {
            return;
        }

        if ($event->type == DeployType::SERVER) {
            Artisan::call(ImportCommand::class, [], $event->output);
        }

        Artisan::call(ExportCommand::class, ['group' => '*'], $event->output);
    }
}
