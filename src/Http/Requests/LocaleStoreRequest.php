<?php

namespace MyHotelBike\Translations\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LocaleStoreRequest extends FormRequest {
    public function rules(): array {
        return [
            'locale' => [
                'required',
                Rule::unique('ltm_translations', 'locale'),
            ],
        ];
    }

    public function attributes(): array {
        return __('translations::models.locale.attributes');
    }
}
