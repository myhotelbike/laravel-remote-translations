<?php

namespace MyHotelBike\Translations\Http\Requests;

use Barryvdh\TranslationManager\Manager;
use Illuminate\Foundation\Http\FormRequest;
use MyHotelBike\Translations\Models\Translation;

class TranslationRequest extends FormRequest {
    public function rules(): array {
        $rules = [];

        $locales = app(Manager::class)->getLocales();
        foreach ($locales as $locale) {
            $rules["$locale.value"] = [
                'nullable',
            ];
        }

        return $rules;
    }

    public function attributes(): array {
        return __('translations::models.translation.attributes');
    }

    public function save(Translation $translation) {
        $values = $this->validated();

        foreach ($values as $locale => $translation_values) {
            $translation = Translation::query()->firstOrNew([
                'group'  => $translation->group,
                'key'    => $translation->key,
                'locale' => $locale,
            ]);

            $translation->value = $translation_values['value'];

            if ($translation->isDirty('value') && config('translations.client.host')) {
                $translation->is_overridden = TRUE;
            }

            $translation->save();
        }
    }
}
