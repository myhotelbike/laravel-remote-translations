<?php

namespace MyHotelBike\Translations\Http\Requests\Api;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class TranslationIndexApiRequest extends FormRequest {
    public function rules(): array {
        return [
            'group'      => [
                'nullable',
            ],
            'locale'     => [
                'nullable',
            ],
            'updated_at' => [
                'nullable',
                'date_format:' . \DateTimeInterface::ATOM,
            ],
            'offset'     => [
                'nullable',
                'integer',
                'min:0',
            ],
            'limit'      => [
                'nullable',
                'integer',
                'min:0',
                'max:100',
            ],
        ];
    }

    public function attributes(): array {
        return __('translations::models.translation.attributes');
    }

    public function filter(Builder $query): void {
        $values = $this->validated();

        if (isset($values['group'])) {
            $query->whereIn('group', Arr::wrap($values['group']));
        }

        if (isset($values['locale'])) {
            $query->whereIn('locale', Arr::wrap($values['locale']));
        }

        if (isset($values['updated_at'])) {
            $updated_at = Carbon::createFromFormat(\DateTimeInterface::ISO8601, $values['updated_at']);
            $query->where('updated_at', '>', $updated_at);
        }
    }

    public function paginate(Builder $query) {
        $values = $this->validated();

        $query->orderBy('updated_at');
        $query->orderBy('id');
        $query->offset($values['offset'] ?? 0);
        $query->limit($values['limit'] ?? 100);

    }
}
