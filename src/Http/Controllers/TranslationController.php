<?php

namespace MyHotelBike\Translations\Http\Controllers;

use Barryvdh\TranslationManager\Manager;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use MyHotelBike\Translations\Http\Requests\TranslationRequest;
use MyHotelBike\Translations\Models\Translation;

class TranslationController extends Controller {

    public function __construct() {
        $singular = Translation::SINGULAR;

        $this->middleware("can:$singular.*.index", ['only' => ['index', 'data']]);
        $this->middleware("can:$singular.*.create", ['only' => ['create', 'store']]);
        $this->middleware("can:show,$singular", ['only' => 'show']);
        $this->middleware("can:edit,$singular", ['only' => ['edit', 'update']]);
        $this->middleware("can:admin,$singular", ['only' => ['getAdmin', 'postAdmin']]);
        $this->middleware("can:delete,$singular", ['only' => 'destroy']);
        $this->middleware("can:restore,$singular", ['only' => 'restore']);
    }

    public function index() {
        return view('translations::translation.index');
    }

    public function data() {
        $query = Translation::query()
            ->access();

        return datatables()
            ->eloquent($query)
            ->addColumn('edit_url', fn($t) => Auth::user()->can('edit', $t) ? route('translation.edit', $t) : NULL)
            ->toJson();
    }

    public function edit(Manager $manager, Translation $translation) {
        $locales = $manager->getLocales();
        $translations = Translation::query()
            ->where('group', $translation->group)
            ->where('key', $translation->key)
            ->get()
            ->keyBy('locale');

        return view('translations::translation.form', ['translation' => $translation, 'locales' => $locales, 'translations' => $translations]);
    }

    public function update(TranslationRequest $request, Translation $translation) {
        $request->save($translation);

        return redirect()->route('translation.index', $translation);
    }
}
