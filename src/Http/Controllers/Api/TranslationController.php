<?php

namespace MyHotelBike\Translations\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use MyHotelBike\Translations\Http\Requests\Api\TranslationIndexApiRequest;
use MyHotelBike\Translations\Models\Translation;

class TranslationController extends Controller {

    public function index(TranslationIndexApiRequest $request) {
        $query = Translation::query();

        $request->filter($query);
        $total = $query->count();

        $request->paginate($query);

        return response()->json([
            'result'       => TRUE,
            'total'        => $total,
            'translations' => $query->get(),
        ]);
    }
}
