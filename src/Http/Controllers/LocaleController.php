<?php

namespace MyHotelBike\Translations\Http\Controllers;

use Barryvdh\TranslationManager\Manager;
use Illuminate\Routing\Controller;
use MyHotelBike\Translations\Http\Requests\LocaleStoreRequest;
use MyHotelBike\Translations\Models\Translation;

class LocaleController extends Controller {

    public function __construct() {
        $singular = Translation::SINGULAR;
        $this->middleware("can:$singular.*.index", ['only' => ['index', 'data']]);
        $this->middleware("can:$singular.*.create", ['only' => ['create', 'store']]);
        $this->middleware("can:$singular.*.destroy", ['only' => 'destroy']);
    }

    public function index(Manager $manager) {
        $locales = $manager->getLocales();

        return view('translations::locale.index', ['locales' => $locales]);
    }

    public function create() {
        return view('translations::locale.form');
    }

    public function store(Manager $manager, LocaleStoreRequest $request) {
        $values = $request->validated();

        $manager->addLocale($values['locale']);

        return redirect()->route('locale.index');
    }

    public function destroy(Manager $manager, string $locale) {
        $manager->removeLocale($locale);

        return redirect()->route('locale.index');
    }
}
