<?php

namespace MyHotelBike\Translations\Models;

use Barryvdh\TranslationManager\Models\Translation as BaseTranslation;
use MyHotelBike\LaravelSupport\Contracts\AppModel;
use MyHotelBike\LaravelSupport\Models\Traits\ModelTrait;

class Translation extends BaseTranslation implements AppModel {
    use ModelTrait;

    public const SINGULAR = 'translation';
    public const ACTIONS = ['index', 'create', 'edit', 'delete', 'destroy'];
    public const MODIFIERS = [];
    public const RELATIONS = [];
    public const ATTRIBUTES = [
        'status' => [
            'history' => 'plain',
        ],
        'locale' => [
            'history' => 'plain',
        ],
        'group'  => [
            'history' => 'plain',
        ],
        'key'    => [
            'history' => 'plain',
        ],
        'value'  => [
            'history' => 'plain',
        ],
    ];

    protected $casts = [
        'updated_at' => 'datetime',
        'created_at' => 'datetime',
    ];

    public function scopeDefaultOrder($query) {
        return $this->scopeOrderByGroupKeys($query, TRUE);
    }

    public function scopeAccess($query) {
        //TODO: Implement access.
    }

    public function toString(): string {
        return "{$this->group}.{$this->key}";
    }
}
