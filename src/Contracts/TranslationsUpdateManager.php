<?php


namespace MyHotelBike\Translations\Contracts;


interface TranslationsUpdateManager {

    public function update(): int;
}
