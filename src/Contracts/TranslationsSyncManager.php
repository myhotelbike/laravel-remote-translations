<?php


namespace MyHotelBike\Translations\Contracts;


interface TranslationsSyncManager {

    public function sync(): int;
}
